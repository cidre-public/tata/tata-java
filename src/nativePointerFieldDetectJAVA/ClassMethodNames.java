package nativePointerFieldDetectJAVA;

import java.util.List;

import soot.SootClass;
import soot.SootMethod;

public class ClassMethodNames {
	public String classname;
	public String methodname;
	public ClassMethodNames(String c, String m) {
		classname = c;
		methodname = m;
	}
	@Override
	public int hashCode() {
		String full = classname + ";" + methodname;
		return full.hashCode();
	}		
	@Override
	public boolean equals(Object obj) {
		if (this == obj) 
			return true;
		if (obj == null) 
			return false;
		if (getClass() != obj.getClass()) 
			return false;
		ClassMethodNames other = (ClassMethodNames) obj;
		if (!classname.equals(other.classname))
			return false;
		if (!methodname.contentEquals(other.methodname))
			return false;
		return true;
	}
	public static ClassMethodNames parseRawString(String raw, List<SootMethod> new_native_methods) {
		for(SootMethod meth : new_native_methods) {
			String cls_name = meth.getDeclaringClass().toString();
			String meth_name = meth.getName();
			
			String corresponding_raw = cls_name + "_" + meth_name;
			corresponding_raw = corresponding_raw.replace(".", "_");

			if(raw.startsWith("Java_"))
				raw = raw.replaceFirst("Java_", "");
			
			if(corresponding_raw.endsWith(raw) && raw.endsWith(meth_name.replace(".", "_"))) {
				return new ClassMethodNames(cls_name, meth_name);
			}
		}
		return null;
	}

	static boolean isSerializable(SootClass cls) {
		// Check if the class is explicitly serializable
		for(SootClass interf : cls.getInterfaces()) {
			if(interf.getName().equals("android.os.Parcelable") ||
					interf.getName().equals("java.io.Serializable")) {
				return true;
			}
		}
		// Else check if one of the implemented interfaces is serializable
		for(SootClass interf : cls.getInterfaces()) {
			if(isSerializable(interf)) {
				return true;		
			}
		}
		// Else check if the superclass is serializable
		SootClass sup = cls.getSuperclassUnsafe();
		if(sup != null) {
			if(isSerializable(sup)) {
				return true;
			}
		}
		return false;
	}
}
