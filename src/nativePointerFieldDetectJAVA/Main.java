package nativePointerFieldDetectJAVA;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.xmlpull.v1.XmlPullParserException;

import soot.jimple.infoflow.Infoflow;
import soot.jimple.infoflow.InfoflowConfiguration;
import soot.jimple.infoflow.InfoflowConfiguration.ImplicitFlowMode;
import soot.jimple.infoflow.InfoflowConfiguration.PathBuildingAlgorithm;
import soot.jimple.infoflow.InfoflowConfiguration.PathReconstructionMode;
import soot.jimple.infoflow.InfoflowConfiguration.StaticFieldTrackingMode;
import soot.jimple.infoflow.android.manifest.ProcessManifest;
import soot.jimple.infoflow.entryPointCreators.DefaultEntryPointCreator;

public class Main {
	
	public static void usage() {
		System.err.println("Error usage: src_or_class_dir json_file -m apk_with_manifest_file [optional_java_lib_path] [--analyze-non-serializable]\n"
				                      + "src_or_class_dir json_file -c main_classname [optional_java_lib_path] [--analyze-non-serializable]");
	}

	public static void main(String[] args) {	
		long start = System.currentTimeMillis();
		if(args.length != 4 && args.length != 5 && args.length != 6) {
			usage(); return;
		}

		Collection<String> entrypoints = new ArrayList<String>();
		if(args[2].equals("-m")) {
			try {
				ProcessManifest manifest = new ProcessManifest(args[3]);		
				for(String method : manifest.getEntryPointClasses()) {					
					String tmp = "<" + method + ": ()>";					
					entrypoints.add(tmp);
				}
				manifest.close();
			} catch (IOException e1) {
				System.err.println("IOException while reading apk file for getting the manifest");
			} catch (XmlPullParserException e1) {
				System.err.println("Parsing exception while reading the manifest");
			}
		} else if (args[2].equals("-c")) {
			entrypoints.add("<"+args[3]+": ()>");
		} else {
			usage(); return;
		}

		boolean analyzeNonSerializable = false;
		String libPath = "/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/rt.jar";
		if(args.length == 5) {
			if(args[4].equals("--analyze-non-serializable")) {
				analyzeNonSerializable = true;
			} else {
				libPath = args[4];
			}
		} else if (args.length == 6) {
			if(args[5].equals("--analyze-non-serializable")) {
				libPath = args[4];
				analyzeNonSerializable = true;
			} else {
				usage(); return;
			}
		}
		
		JSONObject json;
		try {
			Object obj = new JSONParser().parse(new FileReader(args[1]));
			json = (JSONObject) obj; 
		} catch (FileNotFoundException e) {
			System.err.println(args[1] + " not found\n");return;
		} catch (IOException e) {
			System.err.println("IOException while reading " + args[1]);return;
		} catch (ParseException e) {
			System.err.println(args[1] + " not a json\n");return;
		}
		
		/* First analysis:
		 * - Pointer-typed return values are sources
		 * - All fields are sinks
		 * */

		Infoflow infoflow1 = new Infoflow();
		infoflow1.setPostProcessors(Arrays.asList(new PostAnalysisReturnValueTaintedHandler()));

		InfoflowConfiguration infoflowConfig =infoflow1.getConfig();
		InfoflowConfiguration.setPathAgnosticResults(true);
		infoflowConfig.setEnableTypeChecking(false);
		infoflowConfig.getPathConfiguration().setPathReconstructionMode(PathReconstructionMode.NoPaths);
		infoflowConfig.getPathConfiguration().setSequentialPathProcessing(true);
		infoflowConfig.getPathConfiguration().setPathBuildingAlgorithm(PathBuildingAlgorithm.ContextInsensitiveSourceFinder);
		infoflowConfig.setImplicitFlowMode(ImplicitFlowMode.NoImplicitFlows);
		infoflowConfig.setStaticFieldTrackingMode(StaticFieldTrackingMode.None);
		
		infoflow1.computeInfoflow(args[0],
				libPath,
				new DefaultEntryPointCreator(entrypoints),
				new ReturnValueTaintedSourceSinkManager(json, analyzeNonSerializable));

		/* Second analysis:
		 * - Fields are sources
		 * - Pointer-typed method arguments are sinks
		 * */
		
		Infoflow infoflow2 = new Infoflow();
		infoflow2.setPostProcessors(Arrays.asList(new PostAnalysisFieldsTaintedHandler()));

		InfoflowConfiguration infoflowConfig2 =infoflow2.getConfig();
		InfoflowConfiguration.setPathAgnosticResults(true);
		infoflowConfig2.setEnableTypeChecking(false);
		infoflowConfig2.getPathConfiguration().setPathReconstructionMode(PathReconstructionMode.NoPaths);
		infoflowConfig2.getPathConfiguration().setSequentialPathProcessing(true);
		infoflowConfig2.getPathConfiguration().setPathBuildingAlgorithm(PathBuildingAlgorithm.ContextInsensitiveSourceFinder);
		infoflowConfig2.setImplicitFlowMode(ImplicitFlowMode.NoImplicitFlows);
		infoflowConfig2.setStaticFieldTrackingMode(StaticFieldTrackingMode.None);
		
		infoflow2.computeInfoflow(args[0],
				libPath,
				new DefaultEntryPointCreator(entrypoints),
				new FieldsTaintedSourceSinkManager(json, analyzeNonSerializable));

		long end = System.currentTimeMillis();
		System.out.println((end - start) + " ms spent");
	}

}
