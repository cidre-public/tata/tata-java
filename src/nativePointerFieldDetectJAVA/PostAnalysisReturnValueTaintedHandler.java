package nativePointerFieldDetectJAVA;

import soot.jimple.infoflow.handlers.PostAnalysisHandler;
import soot.jimple.infoflow.results.DataFlowResult;
import soot.jimple.infoflow.results.InfoflowResults;
import soot.jimple.infoflow.results.ResultSinkInfo;
import soot.jimple.infoflow.results.ResultSourceInfo;
import soot.jimple.infoflow.solver.cfg.IInfoflowCFG;
import soot.jimple.infoflow.sourcesSinks.definitions.FieldSourceSinkDefinition;
import soot.jimple.infoflow.sourcesSinks.definitions.SourceSinkDefinition;

public class PostAnalysisReturnValueTaintedHandler implements PostAnalysisHandler {

	@Override
	public InfoflowResults onResultsAvailable(InfoflowResults results, IInfoflowCFG cfg) {
		if(results != null && results.getResultSet() != null) {
			for(DataFlowResult dfr : results.getResultSet()) {
				ResultSinkInfo sink = dfr.getSink();
				ResultSourceInfo source = dfr.getSource();
			
				SourceSinkDefinition sinkdef = sink.getDefinition();
				if(sinkdef instanceof FieldSourceSinkDefinition) {
					FieldSourceSinkDefinition fieldsinkdef = (FieldSourceSinkDefinition) sinkdef;

					System.out.print("Source: ");
					System.out.println(source.getDefinition());
					System.out.print("Sink: ");
					System.out.println(fieldsinkdef.getFieldSignature());
				}
			}
		}
		return results;
	}

}
