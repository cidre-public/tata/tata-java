package nativePointerFieldDetectJAVA;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONObject;

import org.json.simple.JSONArray;

import soot.ArrayType;
import soot.PrimType;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Value;
import soot.jimple.DefinitionStmt;
import soot.jimple.FieldRef;
import soot.jimple.Stmt;
import soot.jimple.infoflow.InfoflowManager;
import soot.jimple.infoflow.data.AccessPath;
import soot.jimple.infoflow.data.SootMethodAndClass;
import soot.jimple.infoflow.sourcesSinks.definitions.FieldSourceSinkDefinition;
import soot.jimple.infoflow.sourcesSinks.definitions.MethodSourceSinkDefinition;
import soot.jimple.infoflow.sourcesSinks.manager.ISourceSinkManager;
import soot.jimple.infoflow.sourcesSinks.manager.SinkInfo;
import soot.jimple.infoflow.sourcesSinks.manager.SourceInfo;

public class ReturnValueTaintedSourceSinkManager implements ISourceSinkManager {
	private Set<String> raw_methods;
	private Set<ClassMethodNames> parsed_methods;
	private Set<SootMethod> native_methods;
	private boolean checkNonSerialisableClass;

	public ReturnValueTaintedSourceSinkManager(JSONObject json) {
		this(json, false);
	}

	public ReturnValueTaintedSourceSinkManager(JSONObject json, boolean checkNonSerialisableClass) {
		this.checkNonSerialisableClass = checkNonSerialisableClass;
		raw_methods = new HashSet<String>();
		parsed_methods = new HashSet<ClassMethodNames>();
		native_methods = new HashSet<SootMethod>();
		
		Object o = json.get("pointerReturnValues");
		if(!(o instanceof JSONArray))
			throw new IllegalArgumentException("Json file does not contain an array named pointerReturnValues");

		JSONArray l = (JSONArray) o;
		for(Object m : l) {
			if(!(m instanceof String))
				throw new IllegalArgumentException("Json file pointerReturnValues array does not contain String");

			String raw = (String) m;			
			raw_methods.add(raw);
		}
	}
	
	private void updateParsedMethodsIfNewNativeMethodsLoaded() {
		List<SootMethod> new_native_methods = new ArrayList<SootMethod>();
		Scene scene = Scene.v();
		
		for(SootClass sc : scene.getClasses()) {
			for(SootMethod meth : sc.getMethods()) {
				if(meth.isNative()) {
					if(!native_methods.contains(meth)) {
						native_methods.add(meth);
						new_native_methods.add(meth);
					}
				}
			}
		}
		
		if(new_native_methods.size() > 0) {
			List<String> raw_to_be_removed = new ArrayList<String>();
			for(String raw : raw_methods) {
				ClassMethodNames parsed = ClassMethodNames.parseRawString(raw, new_native_methods);
				if(parsed != null) {
					parsed_methods.add(parsed);
					// The method has been matched successfully with a soot loaded native method
					// We can remove it from raw unmatched methods
					raw_to_be_removed.add(raw);
				}
			}
			raw_methods.removeAll(raw_to_be_removed);
		}
	}
	
	private boolean isPointerMethod(SootMethod m) {
		this.updateParsedMethodsIfNewNativeMethodsLoaded();

		String classname = m.getDeclaringClass().toString();
		String methodname = m.getName();
		if(parsed_methods.contains(new ClassMethodNames(classname, methodname)))
			return true;
		return false;
	}

	@Override
	public void initialize() {}

	@Override
	public SourceInfo getSourceInfo(Stmt sCallSite, InfoflowManager manager) {
		SootMethod callee = sCallSite.containsInvokeExpr() ? sCallSite.getInvokeExpr().getMethod() : null;

		AccessPath targetAP = null;
		if (callee != null) {
			if (isPointerMethod(callee)) {
				if (callee.getReturnType() != null && sCallSite instanceof DefinitionStmt) {
					// Taint the return value
					Value leftOp = ((DefinitionStmt) sCallSite).getLeftOp();
					targetAP = manager.getAccessPathFactory().createAccessPath(leftOp, true);
				}
			}
		}

		if (targetAP == null)
			return null;

		return new SourceInfo(callee == null ? null : new MethodSourceSinkDefinition(new SootMethodAndClass(callee)),
				targetAP);
	}

	@Override
	public SinkInfo getSinkInfo(Stmt sCallSite, InfoflowManager manager,
			AccessPath ap) {
		if(sCallSite instanceof DefinitionStmt) {
			DefinitionStmt def = (DefinitionStmt) sCallSite;
			Value left = def.getLeftOp();
			if(left instanceof FieldRef) {
				FieldRef fieldref = (FieldRef)left;
				SootField field = fieldref.getField();

				if(!this.checkNonSerialisableClass) {
					// Treat only fields from serializable classes
					SootClass cls = field.getDeclaringClass();
					if(!ClassMethodNames.isSerializable(cls))
						return null;
				}
				
				// Omit static fields because they are not serialized
				if(!field.isStatic() && !(field.getType() instanceof ArrayType) && (field.getType() instanceof PrimType)) {	
					return new SinkInfo(new FieldSourceSinkDefinition(field.toString()));
				}
			}
		}
		
		return null;
	}
}
