package nativePointerFieldDetectJAVA;

import soot.jimple.infoflow.handlers.PostAnalysisHandler;
import soot.jimple.infoflow.results.DataFlowResult;
import soot.jimple.infoflow.results.InfoflowResults;
import soot.jimple.infoflow.results.ResultSinkInfo;
import soot.jimple.infoflow.results.ResultSourceInfo;
import soot.jimple.infoflow.solver.cfg.IInfoflowCFG;
import soot.jimple.infoflow.sourcesSinks.definitions.FieldSourceSinkDefinition;
import soot.jimple.infoflow.sourcesSinks.definitions.SourceSinkDefinition;

public class PostAnalysisFieldsTaintedHandler implements PostAnalysisHandler {

	@Override
	public InfoflowResults onResultsAvailable(InfoflowResults results, IInfoflowCFG cfg) {
		if(results != null && results.getResultSet() != null) {
			for(DataFlowResult dfr : results.getResultSet()) {
				ResultSinkInfo sink = dfr.getSink();
				ResultSourceInfo source = dfr.getSource();

				SourceSinkDefinition sourcedef = source.getDefinition();
				if(sourcedef instanceof FieldSourceSinkDefinition) {	
					FieldSourceSinkDefinition fieldsourcedef = (FieldSourceSinkDefinition) sourcedef;

					System.out.print("Source: ");
					System.out.println(fieldsourcedef.getFieldSignature());
					System.out.print("Sink: ");
					System.out.println(sink.toString());
				}
			}
		}
		return results;
	}

}
