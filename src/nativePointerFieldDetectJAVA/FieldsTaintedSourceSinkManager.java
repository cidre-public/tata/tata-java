package nativePointerFieldDetectJAVA;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import soot.ArrayType;
import soot.PrimType;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Value;
import soot.jimple.DefinitionStmt;
import soot.jimple.FieldRef;
import soot.jimple.InvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.infoflow.InfoflowManager;
import soot.jimple.infoflow.data.AccessPath;
import soot.jimple.infoflow.data.SootMethodAndClass;
import soot.jimple.infoflow.sourcesSinks.definitions.FieldSourceSinkDefinition;
import soot.jimple.infoflow.sourcesSinks.definitions.MethodSourceSinkDefinition;
import soot.jimple.infoflow.sourcesSinks.manager.ISourceSinkManager;
import soot.jimple.infoflow.sourcesSinks.manager.SinkInfo;
import soot.jimple.infoflow.sourcesSinks.manager.SourceInfo;

public class FieldsTaintedSourceSinkManager implements ISourceSinkManager {

	private Map<String,Long> raw_args;
	private Map<ClassMethodNames,Long> parsed_args;
	private Set<SootMethod> native_methods;
	private boolean checkNonSerializableClasses;

	public FieldsTaintedSourceSinkManager(JSONObject json) {
		this(json, false);
	}
	
	public FieldsTaintedSourceSinkManager(JSONObject json, boolean checkNonSerializableClasses) {
		raw_args = new HashMap<String,Long>();
		parsed_args = new HashMap<ClassMethodNames,Long>();
		native_methods = new HashSet<SootMethod>();
		this.checkNonSerializableClasses = checkNonSerializableClasses;
		
		Object o = json.get("pointerArgs");
		if(!(o instanceof JSONArray))
			throw new IllegalArgumentException("Json file does not contain an array named pointerArgs");
		
		JSONArray l = (JSONArray) o;
		for(Object m : l) {
			if(!(m instanceof JSONObject)) 
				throw new IllegalArgumentException("Json file pointerArgs array does not contain objects");
			JSONObject metharg = (JSONObject) m;
			
			Object meth = metharg.get("meth");
			if(!(meth instanceof String))
				throw new IllegalArgumentException("Json file pointerArgs array contains object without meth value");
			String methodname = (String) meth;
			
			Object arg = metharg.get("pos");
			if(!(arg instanceof Long))
				throw new IllegalArgumentException("Json file pointerArgs array contains object without pos value");
			// -2 for JNI_env pointer and Class/Object pointer
			Long position = (Long) arg - 2;
			
			// Only aforementioned pointers has been detected
			if(position >= 0) {
				raw_args.put(methodname, position);
			}
		}
	}

	@Override
	public void initialize() {}
	
	@Override
	public SourceInfo getSourceInfo(Stmt sCallSite, InfoflowManager manager) {
		if(sCallSite instanceof DefinitionStmt) {
			DefinitionStmt def = (DefinitionStmt) sCallSite;
			Value right = def.getRightOp();
			if(right instanceof FieldRef) {	
				FieldRef fieldref = (FieldRef)right;
				SootField field = fieldref.getField();

				if(!this.checkNonSerializableClasses) {
					// Treat only fields from serializable classes
					SootClass cls = field.getDeclaringClass();
					if(!ClassMethodNames.isSerializable(cls))
						return null;
				}
				
				// Omit static fields because they are not serialized
				if(!field.isStatic() && !(field.getType() instanceof ArrayType) && (field.getType() instanceof PrimType)) {				
					AccessPath targetAP = null;
					targetAP = manager.getAccessPathFactory().createAccessPath(def.getLeftOp(), true);
					return new SourceInfo(new FieldSourceSinkDefinition(field.toString()), targetAP);
				}
			}
		}
		return null;
	}
	
	private void updateParsedMethodsIfNewNativeMethodsLoaded() {
		List<SootMethod> new_native_methods = new ArrayList<SootMethod>();
		Scene scene = Scene.v();
		
		for(SootClass sc : scene.getClasses()) {
			for(SootMethod meth : sc.getMethods()) {
				if(meth.isNative()) {
					if(!native_methods.contains(meth)) {
						native_methods.add(meth);
						new_native_methods.add(meth);
					}
				}
			}
		}
		
		if(new_native_methods.size() > 0) {
			List<String> raw_to_be_removed = new ArrayList<String>();
			for(String raw : raw_args.keySet()) {
				ClassMethodNames parsed = ClassMethodNames.parseRawString(raw, new_native_methods);
				if(parsed != null) {
					parsed_args.put(parsed, raw_args.get(raw));
					// The method has been matched successfully with a soot loaded native method
					// We can remove it from raw unmatched methods
					raw_to_be_removed.add(raw);
				}
			}
			for(String raw : raw_to_be_removed)
				raw_args.remove(raw);
		}
	}
	
	// Return the param number, -1 if not found
	private long isPointerMethod(SootMethod m) {
		this.updateParsedMethodsIfNewNativeMethodsLoaded();

		String classname = m.getDeclaringClass().toString();
		String methodname = m.getName();
		/* if(raw_args.containsKey(methodname))
			return raw_args.get(methodname); */
		if(parsed_args.containsKey(new ClassMethodNames(classname, methodname)))
			return parsed_args.get(new ClassMethodNames(classname, methodname));
		return -1;
	}
	
	public boolean isSinkAccess(InvokeExpr inv, AccessPath ap) {
		SootMethod callee = inv.getMethod();
		long idx;
		if((idx = isPointerMethod(callee)) != -1) {
			if(inv.getArgs().size() > idx) {
				Value arg = inv.getArgs().get((int)idx);
				return ap == null ? true : (ap.getPlainValue() == null ? true : ap.getPlainValue().equals(arg));
			}
		}
		return false;
	}

	@Override
	public SinkInfo getSinkInfo(Stmt sCallSite, InfoflowManager manager, AccessPath ap) {
		InvokeExpr inv = sCallSite.containsInvokeExpr() ? sCallSite.getInvokeExpr() : null;
		if(inv != null) {
			if (isSinkAccess(inv, ap)) {	
				SootMethod callee = inv.getMethod();		
				return new SinkInfo(new MethodSourceSinkDefinition(new SootMethodAndClass(callee)));
			}
		}
		
		return null;
	}
}
